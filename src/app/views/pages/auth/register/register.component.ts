// Angular
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// RxJS
import { finalize, takeUntil, tap } from 'rxjs/operators';
// Translate
import { TranslateService } from '@ngx-translate/core';
// NGRX
import { Store } from '@ngrx/store';
import { AppState } from '../../../../core/reducers';
// Auth
import { AuthNoticeService, AuthService, Register, User } from '../../../../core/auth/';
import { Subject } from 'rxjs';
import { ConfirmPasswordValidator } from './confirm-password.validator';

import { CountdownConfig, CountdownEvent } from 'ngx-countdown';
import axios from 'axios';
import { environment } from '../../../../../environments/environment';

const KEY = 'time';
const DEFAULT = 60;
@Component({
	selector: 'kt-register',
	templateUrl: './register.component.html',
	encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit, OnDestroy {
	registerForm: FormGroup;
	loading = false;
	otpVerify
	phoneNumber
	refcode 
	otpveri = 0
	bankall
	chanelall
	username
	password
	api = environment.apibackend;
	countTimerStatus = 0
	errors: any = [];
	config: CountdownConfig = {
		leftTime: 60,
		formatDate: ({ date }) => `${date / 1000}`,
		notify:0
	  };

	  

	private unsubscribe: Subject<any>; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

	/**
	 * Component constructor
	 *
	 * @param authNoticeService: AuthNoticeService
	 * @param translate: TranslateService
	 * @param router: Router
	 * @param auth: AuthService
	 * @param store: Store<AppState>
	 * @param fb: FormBuilder
	 * @param cdr
	 */
	constructor(
		private authNoticeService: AuthNoticeService,
		private translate: TranslateService,
		private router: Router,
		private auth: AuthService,
		private store: Store<AppState>,
		private fb: FormBuilder,
		private cdr: ChangeDetectorRef
	) {
		this.unsubscribe = new Subject();
	}

	/*
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
    */

	/**
	 * On init
	 */
	ngOnInit() {
		this.initRegisterForm();
		let value = +localStorage.getItem(KEY)!! ?? DEFAULT;
		console.log(value)
		let phonesend = localStorage.getItem('phonesend');
		let ref = localStorage.getItem('ref');
		console.log(ref)
		if (value <= 0) this.countTimerStatus = 0;
		if (value > 0) this.countTimerStatus = 1;
		this.config = {...this.config, leftTime: value };
		this.phoneNumber = phonesend
		this.refcode = ref
		this.searchBank()
		this.searchChanel()
		this.cdr.detectChanges();
		console.log(this.refcode)

	}

	onStartCount(){
		
	}
	/*
    * On destroy
    */
	ngOnDestroy(): void {
		this.unsubscribe.next();
		this.unsubscribe.complete();
		this.loading = false;
	}

	handleEvent(ev: CountdownEvent) {
		//console.log(ev)
		if (ev.action === 'notify') {
		  // Save current value
		  localStorage.setItem(KEY, `${ev.left / 1000}`);
		  this.otpveri = 1
		
		}
		if(ev.action === 'done'){
			this.countTimerStatus = 0
			
			// this.otpveri = 0
		
			// this.phoneNumber = ''
		}
	  }
	  searchBank(){
		axios({
			method: 'get',
			url: this.api+'/v1/member/bankall',
			})
			.then(response => {  
				console.log(response)
				this.bankall = response.data.message
				
			})
			.catch(err => {
			  console.log(err)
			})
	  }

	  searchChanel(){
		axios({
			method: 'get',
			url: this.api+'/v1/member/chanelall',
			})
			.then(response => {  
				console.log(response)
				this.chanelall = response.data.message
				
			})
			.catch(err => {
			  console.log(err)
			})
	  }

	  resetStop() {
	//	console.log('test')
			var z = this.phoneNumber
    		if(!/^[0-9]+$/.test(z) || z.length <= 9){
				alert("เบอร์โทรศัพท์ของท่านไม่ถูกต้อง กรุณาตรวจสอบ")
				return
			  }
		localStorage.removeItem('ref')
		localStorage.removeItem('phonesend')
		axios({
			method: 'post',
			url: this.api+'/v1/member/sendotp',
			data :{
				"phone":this.phoneNumber,
			}
			})
			.then(response => {  
				console.log(response)
				if(response.data.status == "200"){
					this.refcode = response.data.message
					localStorage.setItem('phonesend', this.phoneNumber);
					localStorage.setItem('ref', this.refcode);
					this.countTimerStatus = 1
					this.config = {...this.config, leftTime: 60 };
					this.otpveri = 1
					this.cdr.detectChanges();
				}else if(response.data.status == 301){
					alert(response.data.message)
				}else{
					alert('ระบบมีปัญหากรุณาติดต่อพนักงาน')
				}
			})
			.catch(err => {
			  console.log(err)
			})
		// this.cdr.detectChanges();
	  }

	  submitotp(){
		  console.log(this.otpVerify)
		  if(this.otpVerify){
		axios({
			method: 'post',
			url: this.api+'/v1/member/verifyotp',
			data :{
				"phone":this.phoneNumber,
				"ref":this.refcode,
				"otp":this.otpVerify
			}
			})
			.then(response => {  
				console.log(response)
				if(response.data.status){
					if(response.data.status =="200"){
						this.otpveri = 2
						this.cdr.detectChanges();
					}else if(response.data.status =="204")	{
					    alert('รหัส OTP ของลูกค้าหมดเวลา กรุณาขอรับรหัส OTP ใหม่อีกครั้ง')
						this.refcode = ''
						this.otpVerify = ''
						this.phoneNumber = ''
						localStorage.removeItem('ref')
						localStorage.removeItem('phonesend')
						this.cdr.detectChanges();
					}else {
						alert('รหัส OTP ผิดกรุณาตรวจสอบให้ละเอียด')
					}
					
				}else{
					alert('ระบบมีปัญหากรุณาติดต่อพนักงาน')
				}
			})
			.catch(err => {
			  console.log(err)
			})
		}else{
			alert('กรุณาใส่รหัส OTP')
		}
	  }
	/**
	 * Form initalization
	 * Default params, validators
	 */
	initRegisterForm() {
		this.registerForm = this.fb.group({
			firstname: ['', Validators.compose([
				Validators.required,
				Validators.minLength(2),
				Validators.maxLength(40)
			])
			],
			lastname: ['', Validators.compose([
				Validators.required,
				Validators.minLength(2),
				Validators.maxLength(40)
			])
			],
			// email: ['', Validators.compose([
			// 	Validators.required,
			// 	Validators.email,
			// 	Validators.minLength(3),
			// 	// https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
			// 	Validators.maxLength(320)
			// ]),
			// ],
			bank: ['', Validators.compose([
				Validators.required
			]),
			],
			channel: ['', Validators.compose([
				Validators.required
			]),
			],
			bankacc: ['', Validators.compose([
				Validators.required,
				Validators.minLength(8),
				Validators.maxLength(15),
				Validators.pattern(/^[0-9]\d*$/)
			]),
			],
			// phone: ['', Validators.compose([
			// 	Validators.required,
			// 	Validators.minLength(10),
			// 	Validators.pattern(/^[0-9]\d*$/)
			// ]),
			// ],
			password: ['', Validators.compose([
				Validators.required,
				Validators.minLength(8),
				Validators.maxLength(16),
				Validators.pattern('^(?=.*[a-zA-Z])[a-zA-Z0-9]+$')
			])
			],
			confirmPassword: ['', Validators.compose([
				Validators.required,
				Validators.minLength(8),
				Validators.maxLength(16),
				Validators.pattern('^(?=.*[a-zA-Z])[a-zA-Z0-9]+$')
			])
			],
		}, {
			validator: ConfirmPasswordValidator.MatchPassword
		});
	}

	/**
	 * Form Submit
	 */
	submit() {
		const controls = this.registerForm.controls;
		console.log(controls)

	if (this.registerForm.invalid) {
			Object.keys(controls).forEach(controlName =>
				controls[controlName].markAsTouched()
			);
			return;
		}
		let ff = controls.password.value
		let result = ff.match(/\d/g)

		if(result == null){
			alert('รหัสผ่านลูกค้าต้องมีทั้งตัวเลขและตัวหนังสือ')
			this.registerForm.controls['password'].setValue('');
			this.registerForm.controls['confirmPassword'].setValue('');
			return;
		}

		axios({
			method: 'post',
			url: this.api+'/v1/member/addmember',
			data :{
				"MEMBERFIRSTNAME":controls.firstname.value,
				"MEMBERLASTNAME":controls.lastname.value,
			    "BANK_ACCOUNT_NUMBER":controls.bankacc.value,
				"CHANELID":controls.channel.value,
				"BANK_ID":controls.bank.value,
				"PASSWORD":controls.password.value,
				"PHONE":this.phoneNumber,
				"REF":this.refcode,
				"OTP":this.otpVerify
			}
			})
			.then(response => {  
				console.log(response)
				if(response.data.status == 200){
					alert(response.data.message)
					this.otpveri = 3
					this.username = response.data.username
					this.password = response.data.password
					this.cdr.detectChanges();
				}else if(response.data.status == 301){
					alert(response.data.message)
					this.router.navigate(['/home']);
				}else if(response.data.status == 300){
					alert(response.data.message)
					window.location.reload();
				}else{
					alert('ระบบมีปัญหากรุณาติดต่อพนักงาน')
					window.location.reload();
				}
			})
			.catch(err => {
			  console.log(err)
			})
	

		// check form
		// if (this.registerForm.invalid) {
		// 	Object.keys(controls).forEach(controlName =>
		// 		controls[controlName].markAsTouched()
		// 	);
		// 	return;
		// }
		// this.loading = true;
		// if (!controls.agree.value) {
		// 	// you must agree the terms and condition
		// 	// checkbox cannot work inside mat-form-field https://github.com/angular/material2/issues/7891
		// 	this.authNoticeService.setNotice('You must agree the terms and condition', 'danger');
		// 	return;
		// }

		// const _user: User = new User();
		// _user.clear();
		// _user.email = controls.email.value;
		// _user.username = controls.username.value;
		// _user.fullname = controls.fullname.value;
		// _user.password = controls.password.value;
		// _user.roles = [];
		// this.auth.register(_user).pipe(
		// 	tap(user => {
		// 		console.log(user)
		// 		// if (user) {
		// 		// 	this.store.dispatch(new Register({authToken: user.accessToken}));
		// 		// 	// pass notice message to the login page
		// 		// 	this.authNoticeService.setNotice(this.translate.instant('AUTH.REGISTER.SUCCESS'), 'success');
		// 		// 	this.router.navigateByUrl('/auth/login');
		// 		// } else {
		// 		// 	this.authNoticeService.setNotice(this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN'), 'danger');
		// 		// }
		// 	}),
		// 	takeUntil(this.unsubscribe),
		// 	finalize(() => {
		// 		this.loading = false;
		// 		this.cdr.markForCheck();
		// 	})
		// ).subscribe();
	}

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.registerForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;
	}
}
