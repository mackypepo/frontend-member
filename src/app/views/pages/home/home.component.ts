import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { PusherService } from 'src/app/pusher.service';
import { MatDialog } from'@angular/material/dialog';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { environment } from '../../../../environments/environment';
import axios from 'axios'
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';
import { FormControl } from '@angular/forms';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'kt-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  displaybankowner: string[] = ['ID','BANK_TITLE','BANK_ACCOUNT_NUMBER','SUM_TODAY','CREATEDATE'];
  displayedhidden: string[] = ['TRANSACTION_DEPOSIT_ID','CREATEDATE','BALANCE',];
  displaytrxde_new: string[] = ['MEMBER_USERNAME','TRANSACTION_GEAR','ACTION_BY','BALANCE','CREATEDATE'];
  token = localStorage.getItem('Token')
  // bank owner
    balacnceUfa : any = 0;
    bankonwer : any;
    trxde_new : any;
    day = new Date().getDate()+1
    setDate = new Date().setDate(this.day)
    date = new Date(this.setDate)
    creatDate = new FormControl(new Date());
    endDate = new FormControl(new Date(this.date));
    api=environment.apibackend;
    linecontact = environment.linecontact;
   
  // table right release
    trxright;
  // data new top/hidden
    trxhidden
  constructor(public dialog: MatDialog,
    private pusherService: PusherService,
    private changeDetectorRefs: ChangeDetectorRef,
    private router: Router,
    private store: Store<AppState>,){
   
  }

  ngOnInit() {
 
    this.preData();
    this.changeDetectorRefs.detectChanges();  
   
  }


  preData(){
    axios({
      method: 'post',
      url: this.api+'/v1/member/checkbalance',
      data :{
				"token":this.token
			}
    })
      .then(response => {
      
       if(response.data.status == 200){
       
       this.balacnceUfa = response.data.data
       this.changeDetectorRefs.detectChanges();  
       }else if(response.data.status == 300 || response.data.status == 404){
       alert(response.data.data)
       }else if(response.data.status == 401){
        alert('มีการล็อคอินใหม่')
				this.store.dispatch(new Logout());
			 }
			 else{
				alert('พบปัญหาในการทำรายการ กรุณาทำรายการใหม่')
			}
    }
      )
      .catch(err => {
        console.error(err)
    
      })
  }

  goToUfabet(){
    axios({
      method: 'post',
      url: this.api+'/v1/member/startufa',
      data :{
				"token":this.token
			}
    })
      .then(response => {
       console.log(response)
       if(response.data.status == 200){
       window.location.href = response.data.data
       }else if(response.data.status == 300 || response.data.status == 404){
       alert(response.data.data)
      }else if(response.data.status == 401){
        alert('มีการล็อคอินใหม่')
				this.store.dispatch(new Logout());
			 }
			 else{
				alert('พบปัญหาในการทำรายการ กรุณาทำรายการใหม่')
			}
    }
      
      )
      .catch(err => {
        console.error(err)
    
      })
  }

  onSupport(){
    window.location.href = this.linecontact
  }

  goToWithdraw($myParam: string = ''): void {
    const navigationDetails: string[] = ['/withdraw'];
    if($myParam.length) {
      navigationDetails.push($myParam);
    }
    this.router.navigate(navigationDetails);
  }

  goToDeposit($myParam: string = ''): void {
    const navigationDetails: string[] = ['/credit'];
    if($myParam.length) {
      navigationDetails.push($myParam);
    }
    this.router.navigate(navigationDetails);
  }

}