import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms'
import axios from 'axios';
import { environment } from '../../../../environments/environment';
import * as moment from 'moment';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'kt-trxlistmember',
  templateUrl: './trxlistmember.component.html',
  styleUrls: ['./trxlistmember.component.scss']
})
export class TrxlistmemberComponent implements OnInit {
  token = localStorage.getItem('Token')
  
  result='Y'

  DataTrxlistmember: string[] = ['MEMBER_USERNAME','LINE_OA','SUM_COUNT_TD','SUM_BALANCE_TD','SUM_COUNT_TW','SUM_BALANCE_TW','SUM_NET'];
  datasource= new MatTableDataSource();
  datasourceSum;
  countTD=0;
  balanceTD=0;
  countTW=0;
  balanceTW=0;
  net=0;
  length=0;
  ssa;
  @ViewChild(MatPaginator) paginator: MatPaginator;
    // datepicker
    // teestdeate = moment().add(1, 'days');
    
    day = new Date().getDate()+1
    setDate = new Date().setDate(this.day)
    date = new Date(this.setDate)
    creatDate = new FormControl(new Date());
    endDate = new FormControl(new Date(this.date));
    websiteAll;
    agentType = "";
    username="";
    // api
    api = environment.apibackend;
  constructor(private changeDetectorRefs: ChangeDetectorRef,
    private store: Store<AppState>) { }

  ngOnInit(): void {
    axios({
			method: 'get',
      url: this.api+'/lineoa/all',
      headers: {
        'Authorization': 'Bearer '+this.token
      },
			})
			.then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
			// do something about response
      this.websiteAll = response.data
     // this.creatDate.setValue(this.daySet)
        }
			})
			.catch(err => {
      console.error(err)
      })
      this.onSerach();
     
  }

   ngAfterViewInit() {
    this.datasource.paginator = this.paginator;
  }

  onSerach(){

    let get_frist_date = this.creatDate.value;
    let frist_date = ("0" + get_frist_date.getDate()).slice(-2);
    let frist_month = ("0" + (get_frist_date.getMonth() + 1)).slice(-2);
    let frist_year = get_frist_date.getFullYear();
    let f_date = frist_year + "-" + frist_month + "-" + frist_date;

    let get_last_date = this.endDate.value;
    let last_date = ("0" + get_last_date.getDate()).slice(-2);
    let last_month = ("0" + (get_last_date.getMonth() + 1)).slice(-2);
    let last_year = get_last_date.getFullYear();
    let l_date = last_year + "-" + last_month + "-" + last_date;

    axios({
      method: 'post',
      url: this.api+'/member/trxlist',
      headers: {
        'Authorization': 'Bearer '+this.token
      },
      data :{
       "USERNAME":this.username,
       "AGENTTYPE":this.agentType,
       "CREATEDATE":f_date,
       "ENDDATE":l_date
       
     }
    })
    .then(response => {
      if(response.data.status ==401){
        alert('มีการล็อคอินใหม่')
        this.store.dispatch(new Logout());
      }else{
      console.log("response: ", response.data)
      // do something about response
    //  this.datasource = response.data.message;
     this.datasourceSum = response.data.message
     this.datasource = new MatTableDataSource(response.data.message)
     this.datasource.paginator = this.paginator;
     this.getTotalAmount();
     this.changeDetectorRefs.detectChanges()
      }
    })
    .catch(err => {
      console.error(err)
    })
  }

  getTotalAmount() {
    this.countTD = this.datasourceSum.map(t => t.SUM_COUNT_TD).reduce((acc, value) => acc + value, 0);
    this.balanceTD = this.datasourceSum.map(t => t.SUM_BALANCE_TD).reduce((acc, value) => acc + value, 0);
    this.countTW = this.datasourceSum.map(t => t.SUM_COUNT_TW).reduce((acc, value) => acc + value, 0);
    this.balanceTW = this.datasourceSum.map(t =>   t.SUM_BALANCE_TW).reduce((acc, value) => acc + value, 0);
    this.net = this.datasourceSum.map(t => t.SUM_NET).reduce((acc, value) => acc + value, 0);
    this.ssa = Number(this.net).toFixed(2)
    this.length = this.datasourceSum.length
    // console.log(countTD + " " +balanceTD+ " " + countTW + " " + balanceTW + " "+ net + " "+length)
    }

}