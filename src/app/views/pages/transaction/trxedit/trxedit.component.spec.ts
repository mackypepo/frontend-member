import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrxeditComponent } from './trxedit.component';

describe('TrxeditComponent', () => {
  let component: TrxeditComponent;
  let fixture: ComponentFixture<TrxeditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrxeditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrxeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
