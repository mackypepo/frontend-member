import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import axios from 'axios'
import { environment } from '../../../../../environments/environment';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';

@Component({
  selector: 'kt-trxdeposit',
  templateUrl: './trxdeposit.component.html',
  styleUrls: ['./trxdeposit.component.scss']
})
export class TrxdepositComponent implements OnInit {
  token = localStorage.getItem('Token')

  displayedColumnssum : string[] = 
  ['CREATEDATE','BALANCE'];
  
  dataSourceSum; 
  Trxdeposit;

  api = environment.apibackend;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private changeDetectorRefs: ChangeDetectorRef,
    private store: Store<AppState> ) { }

  ngOnInit(): void {
    this.onSearch();
    this.changeDetectorRefs.detectChanges();  
  }

  onSearch(){
    axios({
      method: 'post',
      url: this.api+'/v1/member/hisdeposit',
      data :{
				"token":this.token
			}
    })
      .then(response => {
      
       if(response.data.status == 200){
       this.Trxdeposit = new MatTableDataSource(response.data.data)
       this.Trxdeposit.paginator = this.paginator
       this.changeDetectorRefs.detectChanges();  
       }else if(response.data.status == 300 || response.data.status == 404){
       alert(response.data.data)
       }else if(response.data.status == 401){
        alert('มีการล็อคอินใหม่')
				this.store.dispatch(new Logout());
			 }
			 else{
				alert('พบปัญหาในการทำรายการ กรุณาทำรายการใหม่')
			}
    }
      )
      .catch(err => {
        console.error(err)
    
      })
  }
  
}
