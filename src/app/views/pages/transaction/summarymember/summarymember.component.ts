import { Component, OnInit, Inject } from '@angular/core';

export interface TableTrxlist {
	no:number;
  	user: string;
  	name: string;
 	bank: string;
	recommend: string;
	recommendOther:string;
	createdate:string;
}

var Trxlist_DATAtrxlist: TableTrxlist[] = [
 {no: 1, user: 'Hydrogen', name:'sarawut', bank:'kbank',recommend:'google',recommendOther:'Asus',createdate:'24/07/2020'},
];


@Component({
  selector: 'kt-summarymember',
  templateUrl: './summarymember.component.html',
  styleUrls: ['./summarymember.component.scss']
})
export class SummarymemberComponent implements OnInit {

  displayedColumns: string[] = ['no','user','name','bank','recommend','recommendother','createdate','buttonlink','historydeposit','historywithdraw','editTrxlist'];
  dataSource = Trxlist_DATAtrxlist;

 
  constructor() { }

  ngOnInit(): void {
	  console.log(this.dataSource)
  }

  
  openDialog() {

}
}
