import { Component, OnInit,ChangeDetectorRef, Inject, ViewChild} from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { FormControl } from '@angular/forms';
import axios from 'axios'
import { environment } from '../../../../../environments/environment';
import { Logout } from '../../../../core/auth';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'kt-t2rxlist',
  templateUrl: './trxlist.component.html',
  styleUrls: ['./trxlist.component.scss']
})
export class TrxlistComponent implements OnInit {
  token = localStorage.getItem('Token')
  profile =  JSON.parse(localStorage.getItem('Profile'));
  
  displayedColumnTrxlist: string[] = ['ID','MEMBER_USERNAME','MEMBER_FULLNAME','BANKNAME','LINEOA','CHANELNAME','CREATEDATE','historydeposit','historywithdraw','editTrxlist'];
  day = new Date().getDate()+1
  setDate = new Date().setDate(this.day)
  date = new Date(this.setDate)
  creatDate = new FormControl(new Date());
  endDate = new FormControl(new Date(this.date));
  websiteAll;
  username="";
  agentType = "";
  lineoa="";
  listTrx= new MatTableDataSource();
  // DatalistTrx;
  api = environment.apibackend;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  myFuntion() {
    alert("copy already");
    
  }
  
  constructor(public dialog: MatDialog,private changeDetectorRefs: ChangeDetectorRef ,private store: Store<AppState>) { }

  ngOnInit(): void {
    this.trxlistdata();
    this.onWebsiteAll();
    this.changeDetectorRefs.detectChanges();
    
    }


  ngAfterViewInit() {
    this.listTrx.paginator = this.paginator;
  }
W
    onWebsiteAll(){
      axios({
        method: 'get',
        url: this.api+'/lineoa/all',
        headers: {
          'Authorization': 'Bearer '+this.token
          },
        })
        .then(response => {
          if(response.data.status ==401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
        // do something about response
        this.websiteAll = response.data
       // this.creatDate.setValue(this.daySet)
          }
        })
        .catch(err => {
        console.error(err)
       
        })	
    }
    
    ontest(){
      axios({
        method: 'get',
        url: this.api+'/withdraw/all',
        headers: {
          'Authorization': 'Bearer '+this.token
          },
        })
        .then(response => {
          if(response.data.status ==401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
          console.log(response.data)
          // do something about response
          }
        })
        .catch(err => {
          console.log(err)
          
          
        })
      
    }
    
    
    trxlistdata(){
    // alert(1111);
    let get_frist_date = this.creatDate.value;
    let frist_date = ("0" + get_frist_date.getDate()).slice(-2);
    let frist_month = ("0" + (get_frist_date.getMonth() + 1)).slice(-2);
    let frist_year = get_frist_date.getFullYear();
    let f_date = frist_year + "-" + frist_month + "-" + frist_date;
  
    let get_last_date = this.endDate.value;
    let last_date = ("0" + get_last_date.getDate()).slice(-2);
    let last_month = ("0" + (get_last_date.getMonth() + 1)).slice(-2);
    let last_year = get_last_date.getFullYear();
    let l_date = last_year + "-" + last_month + "-" + last_date;
  
    console.log(f_date+' AND '+l_date);
    
      axios({
        method: 'post',
        url: this.api+'/member/agenttype',
        headers: {
          'Authorization': 'Bearer '+this.token
          },
        data :{
          "LINEOA":this.lineoa,
          "USERNAME":this.username,
          "AGENTTYPE":this.agentType,
          "CREATEDATE":f_date,
          "ENDDATE":l_date
        }
        })
        .then(response => {
          if(response.data.status ==401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
          console.log(response.data.message)
        // do something about response
        this.listTrx = new MatTableDataSource(response.data.message)
        // this.creatDate.setValue(this.daySet)
        this.listTrx.paginator = this.paginator
        this.changeDetectorRefs.detectChanges();
        console.log(response.data)
          }
        })
        .catch(err => {
        console.error(err)
        // this.store.dispatch(new Logout());
        })   
     
    }
  


// Open-Dialog-History-Deposit
  openDialogHisDepo(ID) {
    const dialogRef = this.dialog.open(DialogtrxlistDeposit, {
      data: {
        dataKey: ID
      }
    });
    
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      
    });
  }


// end

// Open dialog-trxlist-History-Withdraw

openDialogHisWithd(ID) {
  const dialogRef = this.dialog.open(DialogtrxlistWithdraw, {
    data: {
      dataKey: ID
    }
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log(`Dialog result: ${result}`);
  });
}
// end dialog-trxlist-history-withdraw


// Open dialog-trxlist-edit
openDialogEdit(index) {
  // console.log(index)
  // console.log(this.listTrx)
  const dialogRef = this.dialog.open(DialogtrxlistEdit, {
    data: {
      dataKey: index
    }
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log(`Dialog result: ${result}`);
  this.refresh();
  });
}
// end dialog-trxlist-edit


OnSearch(){
  console.log(this.endDate.value)
  let get_frist_date = this.creatDate.value;
  let frist_date = ("0" + get_frist_date.getDate()).slice(-2);
  let frist_month = ("0" + (get_frist_date.getMonth() + 1)).slice(-2);
  let frist_year = get_frist_date.getFullYear();
  let f_date = frist_year + "-" + frist_month + "-" + frist_date;
  console.log(f_date)

  let get_last_date = this.endDate.value;
  let last_date = ("0" + get_last_date.getDate()).slice(-2);
  let last_month = ("0" + (get_last_date.getMonth() + 1)).slice(-2);
  let last_year = get_last_date.getFullYear();
  let l_date = last_year + "-" + last_month + "-" + last_date;

  // console.log(f_date+' AND '+l_date);
  console.log(this.username)
  console.log(this.agentType)
    axios({
      method: 'post',
      url: this.api+'/member/agenttype',
      headers: {
				'Authorization': 'Bearer '+this.token
			  },
      data :{
        "LINEOA":this.lineoa,
        "USERNAME":this.username,
        "AGENTTYPE":this.agentType,
        "CREATEDATE":f_date,
        "ENDDATE":l_date
      }
      })
      .then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
        console.log(response)
      // do something about response
      this.listTrx = new MatTableDataSource(response.data.message)
      this.listTrx.paginator = this.paginator
      this.changeDetectorRefs.detectChanges();
        }
      })
      .catch(err => {
      console.error(err)
      })   
  }
    refresh() {
     this.trxlistdata();
     this.changeDetectorRefs.detectChanges();
   };



}



// Dialog-trxlist-history-deposit
@Component({
  selector: 'dialog-trxlist-history/dialog-trxlist-history-deposit',
  templateUrl: 'dialog-trxlist-history/dialog-trxlist-history-deposit.html',
  styleUrls: ['./trxlist.component.scss']
  })
  export class DialogtrxlistDeposit {
    token = localStorage.getItem('Token')
    profile =  JSON.parse(localStorage.getItem('Profile'));
    constructor(
      @Inject(MAT_DIALOG_DATA) public data: any,
      public matDialog: MatDialog,
      private store: Store<AppState>
   ) { 

   }
    displayedDialogTrxDeposit: string[] = ['ID','BALANCE','REMARK'];
    api = environment.apibackend;
    dataSourceHisDepo;
    
    ngOnInit(): void {
      console.log(this.data)
      
      axios({
        method: 'get',
        url: this.api+'/deposit/all/'+this.data.dataKey,
        headers: {
          'Authorization': 'Bearer '+this.token
          },
      })
       .then(response => {
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{ 
         
        this.dataSourceHisDepo = response.data
        console.log(this.dataSourceHisDepo)
        }
      })
      .catch(err => {
        console.error(err)
        
      })
      }
    }
  
    

  
  
  // end dialog-trxlist-history-deposit


  // dialog-trxlist-history-withdraw
  @Component({
    selector: 'dialog-trxlist-history/dialog-trxlist-history-withdraw',
    templateUrl: 'dialog-trxlist-history/dialog-trxlist-history-withdraw.html',
    styleUrls: ['./trxlist.component.scss']
    })
    export class DialogtrxlistWithdraw {
      token = localStorage.getItem('Token')
    profile =  JSON.parse(localStorage.getItem('Profile'));
      constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public matDialog: MatDialog,
        private store: Store<AppState>
     ) { 
  
     }

      displayedDialogTrxWithdraw: string[] = ['ID','BALANCE','REMARK'];
      dataSourceHisWithd;
      api = environment.apibackend;
    
    ngOnInit(): void {
      console.log(this.data)
      
      axios({
        method: 'get',
        url: this.api+'/withdraw/all/'+this.data.dataKey,
        headers: {
          'Authorization': 'Bearer '+this.token
          },
      })
       .then(response => { 
        if(response.data.status ==401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
        }else{
         
        this.dataSourceHisWithd = response.data
        console.log(this.dataSourceHisWithd)
        }
      })
      .catch(err => {
        console.error(err)
 
      })
      }
    
    
    }

// end dialog-trxlist-history-withdraw


// dialog-trxlist-edit
@Component({
  selector: 'dialog-trxlist-history/dialog-trxlist-edit',
  templateUrl: 'dialog-trxlist-history/dialog-trxlist-edit.html',
  styleUrls: ['./trxlist.component.scss']
  })
  export class DialogtrxlistEdit {
    token = localStorage.getItem('Token')
    profile =  JSON.parse(localStorage.getItem('Profile'));
    constructor(
      @Inject(MAT_DIALOG_DATA) public data: any,
      public matDialog: MatDialog,
      private store: Store<AppState>
    ) {
      
    }
    
    api = environment.apibackend;
    EditTrxlistData
    BankAll
    ngOnInit(): void {
      axios({
        method: 'get',
        url: this.api+'/bank/all',
        headers: {
          'Authorization': 'Bearer '+this.token
          },
        })
        .then(response => {
          if(response.data.status ==401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
          // do something about response
          this.BankAll = response.data
          console.log(this.BankAll)
          }
        })
        .catch(err => {
          console.error(err)
        })
      console.log(this.data)

     axios ({
        method: 'get',
        url: this.api+'/member/'+ this.data.dataKey,
        headers: {
          'Authorization': 'Bearer '+this.token
          },
        })
        .then(response => {
          if(response.data.status ==401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
          // do something about response
          console.log(response.data)
          this.EditTrxlistData=response.data
          }
        })
        .catch(err => {
          console.error(err)
        })
    }

    async onSubmit(){
      console.log(this.EditTrxlistData)
      await axios({
        method: 'post',
        url: this.api+'/member/edit',
        headers: {
          'Authorization': 'Bearer '+this.token
          },
        data:{
          'ID':this.EditTrxlistData.ID,
          'MEMBER_USERNAME':this.EditTrxlistData.MEMBER_USERNAME,
          'MEMBERFIRSTNAME':this.EditTrxlistData.MEMBERFIRSTNAME,
          'MEMBERLASTNAME':this.EditTrxlistData.MEMBERLASTNAME,
          'PHONE':this.EditTrxlistData.PHONE,
          'BANK_ID':this.EditTrxlistData.BANK_ID,
          'BANK_ACCOUNT_NUMBER':this.EditTrxlistData.BANK_ACCOUNT_NUMBER,
          'BANK_ACCOUNT_NAME':this.EditTrxlistData.BANK_ACCOUNT_NAME,
        }
      })
        .then(response => {
          if(response.data.status ==401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
          console.log("response: ", response.data)
          // do something about response
          if(response.data.message == "Suscess"){
            alert('แก้ไขเรียบร้อย')
            this.matDialog.closeAll();
          }else{
            alert('ไม่สามารถทำรายการได้กรุณารอสักครู่ แล้วทำรายการใหม่')
            this.matDialog.closeAll();
          }
        }
        }) 
        .catch(err => {
          console.error(err)
        })
      }
  
   
  
  }

  
// end dialog-trxlist-edit
  


