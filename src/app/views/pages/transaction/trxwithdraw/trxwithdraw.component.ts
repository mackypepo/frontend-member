import { Component, OnInit ,ChangeDetectorRef, ViewChild} from '@angular/core';
import { MatDialog } from '@angular/material/dialog'
import { DataSource } from '@angular/cdk/table';
import { FormControl } from '@angular/forms';
import axios from 'axios'
import { environment } from '../../../../../environments/environment';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'kt-trxwithdraw',
  templateUrl: './trxwithdraw.component.html',
  styleUrls: ['./trxwithdraw.component.scss']
})
export class TrxwithdrawComponent implements OnInit {
  token = localStorage.getItem('Token')

  displayedColumnsFirstTable: string[] =['CREATEDATE','BALANCE'];
  TrxWithdrawDataTableSecond ;
  transactions;
  actionButton;

  day = new Date().getDate()+1
  setDate = new Date().setDate(this.day)
  date = new Date(this.setDate)
  creatDate = new FormControl(new Date());
  endDate = new FormControl(new Date(this.date));
  
  Trxwithdraw;
  bankowner;
  username="";
  websiteAll;
  agentType = "";
  api = environment.apibackend;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public dialog: MatDialog,
    private changeDetectorRefs: ChangeDetectorRef,
    private store: Store<AppState>
     ) {}

     ngOnInit(): void {
      this.onSearch();
      this.changeDetectorRefs.detectChanges();  
    }
  
    onSearch(){
      axios({
        method: 'post',
        url: this.api+'/v1/member/hiswithdraw',
        data :{
          "token":this.token
        }
      })
        .then(response => {
     
         if(response.data.status == 200){
         this.Trxwithdraw = new MatTableDataSource(response.data.data)
         this.Trxwithdraw.paginator = this.paginator
         this.changeDetectorRefs.detectChanges();  
         }else if(response.data.status == 300 || response.data.status == 404){
         alert(response.data.data)
         }else if(response.data.status == 401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
         }
         else{
          alert('พบปัญหาในการทำรายการ กรุณาทำรายการใหม่')
        }
      }
        )
        .catch(err => {
          console.error(err)
      
        })
    }

}
