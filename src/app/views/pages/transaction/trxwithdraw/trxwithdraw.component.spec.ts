import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrxwithdrawComponent } from './trxwithdraw.component';

describe('TrxwithdrawComponent', () => {
  let component: TrxwithdrawComponent;
  let fixture: ComponentFixture<TrxwithdrawComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrxwithdrawComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrxwithdrawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
