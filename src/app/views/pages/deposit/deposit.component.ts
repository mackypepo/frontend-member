import { Component, OnInit,ElementRef, AfterViewInit, ViewChild } from '@angular/core';

@Component({
  selector: 'kt-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.scss']
})
export class DepositComponent implements OnInit ,AfterViewInit {
    @ViewChild('wizard', {static: true}) el: ElementRef;
  
    model: any = {
      uname: 'John',
      fname: 'Wick',
      phone: '0987654321',
      email: 'john.wick@reeves.com',
      ccname: 'John Wick',
      bank:'thai',
      role:'Supersaiya',
      fpassword:'1234',
    };
    submitted = false;
  
    constructor() {
    }
  
    ngOnInit() {
    }
  
    ngAfterViewInit(): void {
      // Initialize form wizard
      const wizard = new KTWizard(this.el.nativeElement, {
        startStep: 1
        
      });
  
      // Validation before going to next page
      wizard.on('beforeNext', (wizardObj) => {
        // https://angular.io/guide/forms
        // https://angular.io/guide/form-validation
        // validate the form and use below function to stop the wizard's step
        if(this.model.uname === "" || this.model.fname==="" || this.model.fpassword ==="" || this.model.role===""|| this.model.phone==="" ){
         wizardObj.stop();
         alert('กรุณากรอกข้อมูลให้ครบ');
        }
      });
  
      // Change event
      wizard.on('change', (wizard) => {
        setTimeout(() => {
          KTUtil.scrollTop();
        }, 500);
      });
    }
  
    onSubmit() {
      this.submitted = true;
    }
  }
  