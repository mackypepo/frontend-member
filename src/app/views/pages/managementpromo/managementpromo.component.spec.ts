import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementpromoComponent } from './managementpromo.component';

describe('ManagementpromoComponent', () => {
  let component: ManagementpromoComponent;
  let fixture: ComponentFixture<ManagementpromoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagementpromoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementpromoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
