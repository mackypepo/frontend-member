import { Component, OnInit,ElementRef, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormControl } from '@angular/forms'
import axios from 'axios';
import { environment } from '../../../../environments/environment';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Clipboard} from '@angular/cdk/clipboard';


@Component({
  selector: 'kt-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.scss']
})
export class CreditComponent implements OnInit {

@ViewChild('wizard',{static: true}) el: ElementRef;
token = localStorage.getItem('Token')
  id
  dataSource;
  dataSourceSum;
  member;
  submitButton = false ;
  // datepicker
  day = new Date().getDate()+1
  setDate = new Date().setDate(this.day)
  date = new Date(this.setDate)
  creatDate = new FormControl(new Date());
  endDate = new FormControl(new Date(this.date));
  websiteAll;
  agentType = "";
  username="";
  length;
  pageEvent: PageEvent;
  haveValuee : any = '';
  dataRes : any[];
  dataResHave  : any[];
  bankhave
  vvv
  // api
  datetimeend
  api = environment.apibackend;
  mindeposs = environment.mindeposit

  @ViewChild(MatPaginator) paginator: MatPaginator;
 
  model:any={
    id:'',
    balance :'',
    bankname : '',
    create : '',
    update:'',
  }

  promotionDropdown = new FormControl();
  selectFormControl = new FormControl();
  Chosefor:string;
  
    seasons: any[] = [
      {name: 'ธนาคารกสิกรไทย' , value:'KBANK' ,color :'green'},
      {name: 'ธนาคารไทยพาณิชย์', value:'SCB' ,color :'purple'},
    ];

  
  constructor(private changeDetectorRefs: ChangeDetectorRef,private store: Store<AppState>,private clipboard: Clipboard,private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.preData();
    this.changeDetectorRefs.detectChanges();
  }

  ngAfterViewInit() {
  
  }

  

    async onsubmit(){
      console.log(this.model)
      
      if(this.model.balance >= this.mindeposs){

        var dd = Math.floor(Math.random() * 100) / 100
        var cc = this.model.balance+dd
        var gg = cc.toFixed(2)
      axios({
        method: 'post',
        url: this.api+'/v1/member/createdeposit',
        data:{ 
                'token':this.token,
                'id':this.model.id,
                'balance':gg
              }
      })
        .then(response => {
          if(response.data.status == 401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
            if(response.data.status == 200){
            this.preData();
            alert('ทำรายการเรียบร้อย')
         //   this.onClearData();
            }else{
              alert('พบปัญหาในการทำรายการ กรุณาทำรายการใหม่')
            }
          }
         
        
        })
        .catch(err => {
          console.error(err)
        //  this.store.dispatch(new Logout());
        })
      }else{
        alert('ยอดเงินที่ฝากต้องเกิน ' +this.mindeposs+  ' บาท')
      }
    }

    async onCancel(){
      // console.log(this.dataResHave.ID)
      // let id = this.dataResHave.ID
      axios({
        method: 'post',
        url: this.api+'/v1/member/canceldeposit',
        data:{ 
                'token':this.token,
                'id':this.id,
              }
      })
        .then(response => {
          if(response.data.status == 401){
            alert('มีการล็อคอินใหม่')
            this.store.dispatch(new Logout());
          }else{
            if(response.data.status == 200){
            alert('ทำรายการเรียบร้อย')
            window.location.reload();
         //   this.onClearData();
            }else{
              alert('พบปัญหาในการทำรายการ กรุณาทำรายการใหม่')
              
            }
          }
        })
        .catch(err => {
          console.error(err)
        //  this.store.dispatch(new Logout());
        })
      }


   preData(){
     axios({
        method: 'post',
        url: this.api+'/v1/member/getdeposit',
        data :{
          "token":this.token
        }
      })
        .then(response => {
        
         if(response.data.status == 202){
         this.dataRes = response.data.data
        
         this.changeDetectorRefs.detectChanges();
         }else if(response.data.status == 200){
          this.haveValuee = 1
          this.dataResHave = response.data.data[0]
          this.id = response.data.data[0].ID
          this.bankhave = response.data.data[0].bank[0]
          this.vvv = response.data.data[0].bank[0].BANK_ACCOUNT_NUMBER
          let befdatetime = response.data.data[0].UPDATEDATE
          let bg = new Date(befdatetime);
          let cv = bg.setMinutes(bg.getMinutes() + 10);
          this.datetimeend = cv
          this.changeDetectorRefs.detectChanges();
         }else if(response.data.status == 300 || response.data.status == 404){
         alert(response.data.data)
         this.changeDetectorRefs.detectChanges();
        }else if(response.data.status == 401){
          alert('มีการล็อคอินใหม่')
          this.store.dispatch(new Logout());
         }
         else{
          alert('พบปัญหาในการทำรายการ กรุณาทำรายการใหม่')
        }
      
      }
        )
        .catch(err => {
          console.error(err)
      
        })
     }

     onCopy(){
      let ff = this.vvv;
       this.clipboard.copy(ff);
      this._snackBar.open('คัดลอกเลขบัญชีเรียบร้อย !!!!', '',{
        duration: 2000
      });
     }

    refresh() {
        this.preData();
        this.changeDetectorRefs.detectChanges();
      };

    }