import { Component, OnInit, ElementRef, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import axios from 'axios'
import { environment } from '../../../../environments/environment';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';

@Component({
  selector: 'kt-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.scss']
})
export class WithdrawComponent implements OnInit, AfterViewInit {

  @ViewChild('wizard', {static: true}) el: ElementRef;
  token = localStorage.getItem('Token')

    displayDeposit:string[]=['ID','BALANCE','REMARK']
	dataSoure;
	submitButton = false;
	api = environment.apibackend;
	minwithdraw = environment.minwithdraw
	sbb;
	theCheckbox = false;
	dataS : any;
	amount
	submitted = false;
	status = 200
	value ;

	constructor(private changeDetectorRefs: ChangeDetectorRef,
		private store: Store<AppState>) {
	}

	ngOnInit() {
		this.onSerach();
		this.changeDetectorRefs.detectChanges();
	}

	ngAfterViewInit(): void {

	}

	onSubmit(){
		console.log(this.dataS.data)
		// if(this.amount >= this.minwithdraw && this.amount <= this.dataS.data){
		if(this.amount <= this.dataS.data && this.amount >= this.minwithdraw){
			axios({
				method: 'post',
				url: this.api+'/v1/member/createwithdraw',
				data:{ 
						'token':this.token,
						'balance':this.amount
					  }
			  })
				.then(response => {
				  if(response.data.status ==401){
					alert('มีการล็อคอินใหม่')
					this.store.dispatch(new Logout());
				  }else{
					
					if(response.data.status == 200){
					this.onSerach();
					alert('ทำรายการเรียบร้อย')
					this.changeDetectorRefs.detectChanges();
					}else{
					  alert('พบปัญหาในการทำรายการ กรุณาทำรายการใหม่')
					}
				  }

				})
				.catch(err => {
				  console.error(err)
				//  this.store.dispatch(new Logout());
				})

		}else{
			alert(
				'ยอดเงินที่จะถอนต่ำกว่า '+ this.minwithdraw +'.00 บาท ไม่สามารถถอนให้ได้ หรือยอดถอนมีมากกว่าจำนวนเงิน'
			)
		}

	}


	onAccept(){
		axios({
			method: 'post',
			url: this.api+'/v1/member/updatewithdraw',
			data :{
					  "token":this.token
				  }
		  })
			.then(response => {
			
			 if(response.data.status == 202){
			 this.changeDetectorRefs.detectChanges();
			 }else if(response.data.status == 200){
			this.onSerach();
			  this.changeDetectorRefs.detectChanges();
			 }else if(response.data.status == 300 || response.data.status == 404){
			 alert(response.data.data)
			 this.changeDetectorRefs.detectChanges();
			 }else if(response.data.status == 401){
				alert('มีการล็อคอินใหม่')
				this.store.dispatch(new Logout());
			 }
			 else{
				alert('พบปัญหาในการทำรายการ กรุณาทำรายการใหม่')
			}
		  
		  }
			)
			.catch(err => {
			  console.error(err)
		  
		})
	}


	 onSerach(){
		axios({
			method: 'post',
			url: this.api+'/v1/member/checkwithdraw',
			data :{
					  "token":this.token
				  }
		  })
			.then(response => {
			 
			 if(response.data.status == 202){
			 this.dataS = response.data
			 this.status = 202
			 this.changeDetectorRefs.detectChanges();
			 }else if(response.data.status == 200){
			  this.dataS = response.data
			  this.amount = this.dataS.data
			  this.status = 200
			  this.changeDetectorRefs.detectChanges();
			  if(response.data.data >= 100){
				this.submitButton = true
			  }
			  this.changeDetectorRefs.detectChanges();
			 }else if(response.data.status == 300 || response.data.status == 404){
			 alert(response.data.data)
			 this.changeDetectorRefs.detectChanges();
			}else if(response.data.status == 401){
				alert('มีการล็อคอินใหม่')
				this.store.dispatch(new Logout());
			 }
			 else{
				alert('พบปัญหาในการทำรายการ กรุณาทำรายการใหม่')
			}
		  
		  }
			)
			.catch(err => {
			  console.error(err)
		  
			})
		}
}
