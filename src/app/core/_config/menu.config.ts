export class MenuConfig {
  public defaults: any = {
    header: {
      self: {},
      items: [
        {
          title: 'หน้าหลัก',
          root: true,
          icon: 'fas fa-home',
          page: '/home'
        },
      ]
    },
    aside: {
      self: {},
      items: [
        {
          title: '1. หน้าหลัก',
          root: true,
          icon: 'fas fa-home',
          page: '/home'
        },
        {
          title: '2. ฝากเงิน',
          root: true,
          icon: 'far fa-money-bill-alt',
          page: '/credit'
        },
        {
          title: '3. ถอนเงิน',
          root: true,
          icon: 'far fa-money-bill-alt',
          page: '/withdraw'
        },
        {
          title: '4. ประวัติการทำรายการ',
          root: true,
          bullet: 'dot',
          icon: 'far fa-money-bill-alt',
          submenu: [
            {
              title: '4.1 ประวัติการฝาก',
              page: '/transaction/trxdeposit'
            },
            {
              title: '4.2 ประวัตการถอน',
              page: '/transaction/trxwithdraw'
            },
          ]
        }
      ]
    },
  };

  public get configs(): any {
    return this.defaults;
  }
}
